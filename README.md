# papirus-folders-leaf

This provides folders in the [KDE Leaf](https://github.com/qewer33/leaf-kde) dark and light
green colors to the [Papirus](https://github.com/PapirusDevelopmentTeam/papirus-folders) icon
theme.

![](Papirus-Leaf-Dark/64x64/places/folder.svg)
![](Papirus-Leaf-Dark/64x64/places/folder-documents.svg)
![](Papirus-Leaf-Dark/64x64/places/folder-downloads.svg)
![](Papirus-Leaf-Dark/64x64/places/folder-mail.svg)
![](Papirus-Leaf-Dark/64x64/places/folder-music.svg)
![](Papirus-Leaf-Dark/64x64/places/folder-pictures.svg)

![](Papirus-Leaf-Light/64x64/places/folder.svg)
![](Papirus-Leaf-Light/64x64/places/folder-documents.svg)
![](Papirus-Leaf-Light/64x64/places/folder-downloads.svg)
![](Papirus-Leaf-Light/64x64/places/folder-mail.svg)
![](Papirus-Leaf-Light/64x64/places/folder-music.svg)
![](Papirus-Leaf-Light/64x64/places/folder-pictures.svg)

It is inspired by [papirus-colors](https://github.com/varlesh/papirus-colors) but in contrast
uses hardcoded folder colors to make it work outside of KDE Plasma.

## Installation

Copy the `Papirus-Leaf-$THEME` folders to `~/.local/share/icons/`.

## Credits

This mostly just puts together pieces from different repos. All actual work was done by the
[papirus development theme](https://github.com/PapirusDevelopmentTeam) for the icons and by
[@qewer33](https://github.com/qewer33) for the colors.
